import Vue from 'vue'
import Vuex from 'vuex'
import app from "./modules/app"
import createVuexAlong from "vuex-along"
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {
  },
  modules: {
    app
  },
  plugins: [createVuexAlong()]
})
export default store
