import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../layout/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/login')
  },
  {
    path: '/index',
    name: 'index',
    component: index,
    redirect: "/sportsManagement",
    children: [
      {
        path: '/sportsManagement',
        name: 'sportsManagement',
        component: () => import('@/views/sportsManagement'),
        meta: {
          isKeepAlive: false
        }
      },
      {
        path: '/sportsDetail',
        name: 'sportsDetail',
        component: () => import('@/views/sportsDetail'),
        meta: {
          isKeepAlive: false
        }
      },
      {
        path: '/TextScoreManagement',
        name: 'TextScoreManagement',
        component: () => import('@/views/TextScoreManagement'),
        meta: {
          isKeepAlive: false
        }
      },
      {
        path: '/markManagement',
        name: 'markManagement',
        component: () => import('@/views/markManagement'),
        meta: {
          isKeepAlive: false
        }
      },
      {
        path: '/personalManagement',
        name: 'personalManagement',
        component: () => import('@/views/personalManagement'),
        meta: {
          isKeepAlive: false
        }
      },
      {
        path: '/classManagement',
        name: 'classManagement',
        component: () => import('@/views/classManagement'),
        meta: {
          isKeepAlive: false
        }
      },
    ]
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
